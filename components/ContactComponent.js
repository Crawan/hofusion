import React, { Component } from 'react';
import {View, Text } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import { MailComposer } from 'expo';


class Contact extends Component {

    static navigationOptions = {
        title: 'Contact'
    };

    sendMail() {
        MailComposer.composeAsync({
            recipients: ['confusion@food.net'],
            subject: 'Enquiry',
            body: 'To whom it may concern:'
        })
    }

    render() {
        return(
           
			 <View>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>  
                <Card title="Contact Information">
			 		
			 		
                    <Text h1>121, Clear Water Bay Road </Text>
                     	<Text h1>Clear Water Bay, Kowloon </Text>
						<Text h1> HONG KONG</Text>
						<Text h1>Tel: +852 1234 5678 </Text>
						<Text h1>Fax: +852 8765 4321 </Text>
                        <Text h1>Email:confusion@food.net </Text>
                        <Button
                            title="Send Email"
                            buttonStyle={{ backgroundColor: "#512DA8" }}
                            icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
                            onPress={this.sendMail}
                        />
					
                    </Card>
                </Animatable.View>
              </View>);
    }
}

export default Contact;