import React, { Component } from 'react';
import { View, FlatList, Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
//to better rendering the images , we will use Tile instead of ListItem
import { Tile } from 'react-native-elements';
import { DISHES } from '../shared/dishes';
import { ListItem } from 'react-native-elements';

//w'll import connect in order to connect aboutus component to the store
//import { connect } from 'react-redux';
//import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';


//in order to obatain the the state from the react store w'll use ..
const mapStateToProps = state => {
    return {
        dishes: state.dishes,
       
    };
}


class Menu extends Component {
    constructor(props) {
                super(props);
                this.state = {
                        dishes: DISHES
                       };
            }
   

     static navigationOptions = {
        title: 'Menu'
    };

    render() {
        const renderMenuItem = ({ item, index }) => {
            return (

                //<Tile
                <Animatable.View animation="fadeInRightBig" duration={2000}>
                <ListItem
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    hideChevron={true}
                    onPress={ () => navigate('Dishdetail', { dishId: item.id }) }
                    leftAvatar={ { source: require('./images/uthappizza.png') } }
                   /* caption={item.description}
                    featured
                    onPress={() => navigate('Dishdetail', { dishId: item.id })}
                    imageSrc={{ uri: baseUrl + item.image }}*/
                    />
                </Animatable.View >
            );
        }

        const { navigate } = this.props.navigation; 

       /* if (this.props.dishes.isLoading) {
            return (
                <Loading />
            );
        }
        else if (this.props.dishes.errMess) {
            return (
                <View>
                    <Text>{this.props.dishes.errMess}</Text>
                </View>
            );
        } else {*/

            return (
                <FlatList
                    //data={this.props.dishes.dishes}
                    data={this.state.dishes}
                    renderItem={renderMenuItem}
                    keyExtractor={item => item.id.toString()}
                />
            );
        //}
       
        

    }


}

//connect the menu to redux store 
//export default connect(mapStateToProps)(Menu);
export default Menu;