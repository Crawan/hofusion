import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import { Card } from 'react-native-elements';
import { ListItem } from 'react-native-elements';
import { FlatList } from 'react-native';
import { Loading } from './LoadingComponent';
import { LEADERS } from '../shared/leaders';
import * as Animatable from 'react-native-animatable';
//w'll import connect in order to connect aboutus component to the store
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';


//in order to obatain the the state from the react store w'll use ..
/*const mapStateToProps = state => {
    return {
        leaders: state.leaders
    }
}*/


function History(){
    return(
        <Card title="Our history">
        <Text style={{margin: 10}}>
            121, Clear Water Bay Road
            Started in 2010, Ristorante con Fusion quickly established itself as a culinary icon par excellence in Hong Kong. With its unique brand of world fusion cuisine that can be found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.  Featuring four of the best three-star Michelin chefs in the world, you never know what will arrive on your plate the next time you visit us.

            </Text>
        <Text style={{margin: 10}}>
The restaurant traces its humble beginnings to The Frying Pan, a successful chain started by our CEO, Mr. Peter Pan, that featured for the first time the world's best cuisines in a pan.

        </Text>
    </Card>  
    );
}

class AboutUs extends Component {
    constructor(props) {
             super(props);
             this.state = {
                        leaders: LEADERS
              };
          }

    static navigationOptions = {
        title: 'About Us'
    };

   
    render() {
        const { navigate } = this.props.navigation;
        //const { params} = this.props.navigation.state;

        //const renderLeader = ({item, index}) => {
        const renderMenuItem = ({ item, index }) => {

            return (
                <ListItem roundAvatar
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    subtitleNumberOfLines={15}
                    hideChevron={true}
                    leftAvatar={{ source: require('./images/alberto.png') }}
                    /*In order to have the image direct from the store , w'll use :( uri: baseUrl + item.image )
                    leftAvatar={{ source: { uri: baseUrl + item.image } }}*/
                />
            );
        };

        //Indecator ...
        /*if (this.props.leaders.isLoading) {
            return (
                <ScrollView>
                    <History />
                    <Card
                        title='Corporate Leadership'>
                        <Loading />
                    </Card>
                </ScrollView>
            );
        }
        else if (this.props.leaders.errMess) {
            return (
                <ScrollView>
                 <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />
                    <Card
                        title='Corporate Leadership'>
                        <Text>{this.props.leaders.errMess}</Text>
                    </Card>
                    </Animatable.View>
                </ScrollView>
            );
        }*/
        
            return (
                <ScrollView>
                    <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />

                    <Card title="Corporate leadership">
                        <FlatList
                            //data={this.props.leaders.leaders}
                            //renderItem={renderLeader}
                            data={this.state.leaders}
                            renderItem={renderMenuItem}
                            keyExtractor={item => item.id.toString()}
                        />
                        </Card>
                    </Animatable.View>
                </ScrollView>
            );
       
    }
}

// to connect aboutus component to our redux store w'll use CONNECT fucnion ..
//export default connect(mapStateToProps)(AboutUs);
export default AboutUs;