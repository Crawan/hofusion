import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, Modal, StyleSheet, Button, Alert, PanResponder, Share } from 'react-native';

import { Card, Icon, Input, Rating } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import { COMMENTS } from '../shared/comments.js';
import { DISHES } from '../shared/dishes';

//w'll import connect in order to connect aboutus component to the store
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postComment, postFavorite } from '../redux/ActionCreators';



/*in order to obatain the the state from the react store w'll use ..
const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites
    }
}*/

/*const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId))
});*/




function RenderDish(props) {

    const dish = props.dish;
    handleViewRef = ref => this.view = ref;
    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        if (dx < -200)
            return true;
        else
            return false;
    };

    const recognizeComment = ({ dx }) => {
        if (dx > 200)
            return true;
        else
            return false;
    };

    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ': ' + message + ' ' + url,
            url: url
        }, {
            dialogTitle: 'Share ' + title
        })
    }

  

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },

        onPanResponderGrant: () => {
            this.view.rubberBand(1000).then(endState =>
                console.log(endState.finished ? 'finished' : 'cancelled'));
         },

        onPanResponderEnd: (e, gestureState) => {
            console.log("pan responder end", gestureState);
            if (recognizeDrag(gestureState))
                Alert.alert(
                    'Add Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to favorite?',
                    [
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        { text: 'OK', onPress: () => { props.favorite ? console.log('Already favorite') : props.onPress() } },
                    ],
                    { cancelable: false }

                );
            else if (recognizeComment(gestureState)) {
                openCommentForm();
            }
            return true;
         }

            
    })    
        if (dish != null) {
            return (

                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
                    ref={this.handleViewRef}
                    {...panResponder.panHandlers}>

                <Card

                    featuredTitle={dish.name}
                    image={require('./images/uthappizza.png')}
                /*image={{ uri: baseUrl + dish.image }}*/>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                    <Icon
                        //raised display the Icon in the form of a button 
                        raised
                        //resvers show differnt color 
                        reversed
                        //the name of the Icon 
                        name={props.favorite ? 'heart' : 'heart-o'}
                        type='font-awesome'
                        color='f50'
                        //the event with onPress
                            onPress={() => props.favorite ? console.log('Aleardy favorite') : props.onPress()} />
                        <Icon
                            raised
                            reverse
                            name='share'
                            type='font-awesome'
                            color='#51D2A8'
                            style={styles.cardItem}
                            onPress={() => shareDish(dish.name, dish.description)} />
                    </Card>
                </Animatable.View>
                    
            );
        }
        else {
            return(<View></View>);
        }
}

function RenderComments(props) {
    const comments = props.comments;
    const renderCommentItem = ({ item, index }) => {
        return (
            <View key={index} style={{ margin: 10 }}>
                <Text style={{ fontSize: 14 }}>{item.comment}</Text>
                <Text style={{ fontSize: 12 }}>{item.rating} Stars</Text>
                <Text style={{ fontSize: 14 }}>{'--- '+ item.author +', ' + item.date}></Text>
            </View >
        );
    }

    //to render all the comments ...
    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}> 
        <Card title='Comments'>
            <FlatList data={comments}
                renderItem={renderCommentItem}
                keyExtractor={item => item.id.toString()}/>
            </Card>
            </Animatable.View >
        );
};


class Dishdetail extends Component {
    constructor(props) {
        super(props);
        this.state  = {
            dishes: DISHES,
                comments: COMMENTS,
                    favorites: []
        }
        //this.state = this.defaultState();
    };

    markFavorite(dishId) {
        this.props.postFavorite(dishId);
    }

    static navigationOptions = {
        title: 'Dish Details'
    }; 

    defaultState() {
        return ({
            rating: 3,
            author: '',
            comment: '',
            showCommentForm: false,
        })
    }


    resetCommentForm() {
        this.setState(this.defaultState());
    }

    handleComment(dishId) {
        this.props.postComment(dishId, this.state.rating, this.state.author, this.state.comment);
        this.resetCommentForm();
    }

    openCommentForm() {
        this.setState({ showCommentForm: true })
    }

    setRating(rating) {
        this.setState({ rating })
    }

    setAuthor(author) {
        this.setState({ author })
    }

    setComment(comment) {
        this.setState({ comment })
    }

    render() {
        const dishId = this.props.navigation.getParam('dishId','');
        return (
            <ScrollView>
                <RenderDish dish={this.state.dishes[+dishId]}
                            //some will return a true if an item found in there
                            favorite={this.state.favorites.some(el => el === dishId)}
                    onPress={() => this.markFavorite(dishId)}
                /*RenderDish dish={this.props.dishes.dishes[+dishId]}
                   //some will return a true if an item found in there
                  favorite={this.props.favorites.some(el => el === dishId)}
                  onPress={() => this.markFavorite(dishId)}
              *//>
                <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.showCommentForm}
                    onDismiss={() => { this.resetCommentForm() }}
                    onRequestClose={() => { this.resetCommentForm() }}
                >
                    <View style={styles.modal}>
                        <Text style={styles.modalTitle}>Add Your Comment</Text>
                        <Rating
                            minValue={1}
                            startingValue={3}
                            fractions={0}
                            showRating={true}
                            onFinishRating={(rating) => this.setRating(rating)}
                        />
                        <Input
                            placeholder="Author"
                            leftIcon={
                                <Icon
                                    name='user'
                                    type='font-awesome'
                                />
                            }
                            onChangeText={(author) => this.setAuthor(author)}
                        />
                        <Input
                            placeholder="Comment"
                            leftIcon={
                                <Icon
                                    name='comment'
                                    type='font-awesome'
                                />
                            }
                            onChangeText={(comment) => this.setComment(comment)}
                        />
                        <Button
                            onPress={() => { this.handleComment(dishId) }}
                            color='#512DA8'
                            title='SUBMIT'
                        />
                        <Button
                            onPress={() => { this.resetCommentForm() }}
                            color='#6c757d'
                            title='CANCEL'
                        />
                    </View>
                </Modal>
                <RenderComments comments={this.state.comments.filter((comment) => comment.dishId === dishId)}
                /*comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)}*//>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    },
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    }
})
//export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);
export default Dishdetail;