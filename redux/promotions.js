//create the reducer for the promotions ..
import * as ActionTypes from './ActionTypes';
//we export the promotions reducer from here  by taking  the state and action as params..
export const promotions = (state = {
    isLoading: true,
    errMess: null,
    promotions: []
}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_PROMOS:
             //we are reseting the follwing...
            return { ...state, isLoading: false, errMess: null, promotions: action.payload };

        case ActionTypes.PROMOS_LOADING:
            return { ...state, isLoading: true, errMess: null, promotions: [] }

        case ActionTypes.PROMOS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload };

        default:
            return state;
    }
};

